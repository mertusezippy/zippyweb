import React from "react";
import "./Left.css";
type Props = {
  campaigns?: any;
};

export const Left = ({ campaigns }: Props) => {
  return (
    <div className="left">
      <div className="topRow">
        <span className="title">HIZLI ÖDE</span>
        <div className="circle">
          <span className="zippy">Zippy</span>
        </div>
      </div>
      <div className="topRow">
        {campaigns[0] && (
          <div className="campaign">
            <p>
              {campaigns[0].availableDays.Monday && (
                <span className="hashtag"> Pazartesi</span>
              )}
              {campaigns[0].availableDays.Tuesday && (
                <span className="hashtag"> Salı</span>
              )}
              {campaigns[0].availableDays.Wednesday && (
                <span className="hashtag"> Çarşamba</span>
              )}
              {campaigns[0].availableDays.Thursday && (
                <span className="hashtag">Perşembe</span>
              )}
              {campaigns[0].availableDays.Friday && (
                <span className="hashtag">Cuma</span>
              )}
              {campaigns[0].availableDays.Saturday && (
                <span className="hashtag">Cumartesi</span>
              )}
              {campaigns[0].availableDays.Sunday && (
                <span className="hashtag">Pazar</span>
              )}
              <span className="hashtag">, Günlerinde</span>
            </p>
            {campaigns[0].lowerLimit && (
              <span className="hashtag">
                {campaigns[0].lowerLimit} TL Üzeri Harcamalarda Geçerli
              </span>
            )}
            <span className="hashtag">{campaigns[0].discount + "TL"}</span>
          </div>
        )}
      </div>
      <div className="topRow">
        <div
          style={{
            flexDirection: "column",
            display: "flex",
            alignItems: "center",
          }}
        >
          <span className="title">İNDİRİM</span>
          <span className="title">HEMEN KULLAN</span>
        </div>
      </div>
      <div className="bottomRow">
        <span className="hashtag">#yenitrendzippy</span>
      </div>
    </div>
  );
};
