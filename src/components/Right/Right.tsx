import React from "react";
import "./Right.css";
import piazza from "../../assets/5f50ba6db761d845e8867d6a.png";
import yasam from "../../assets/5f50bc2ab761d845e8867d6b.png";
import bigChef from "../../assets/5f50bfb2a81d3a19ac51fc90.png";

type Props = {
  qr?: string;
};

export default function Right({ qr }: Props) {
  return (
    <div className="right">
      <img
        className="qr"
        src={
          qr == "5f50ba6db761d845e8867d6a"
            ? piazza
            : qr == "5f50bc2ab761d845e8867d6b"
            ? yasam
            : bigChef
        }
      ></img>
    </div>
  );
}
