import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "../pages/Login/Login";
import Dashboard from "../pages/Dashboard/Dashboard";
export default function Routes() {
  return (
    <Switch>
      <Route path="/dashboard" component={Dashboard} isPrivate />
      <Route component={Home} />
    </Switch>
  );
}
