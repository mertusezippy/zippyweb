import React from "react";
import "./App.css";
import { Router } from "react-router-dom";
import Routes from "./routes/Route";
import history from "./services/history";

function App() {
  return (
    <Router history={history}>
      <Routes />
    </Router>
  );
}

export default App;
