import React, { useState } from "react";
import "./Login.css";
import { useHistory } from "react-router-dom";

function Login() {
  const [credentials, setCredentials] = useState<any>({
    email: null,
    password: null,
  });
  const history = useHistory();

  const loginCall = () => {
    let email = credentials.email;
    let password = credentials.password;
    let url =
      "http://zippyserverappstage-env.eba-wg3eav4w.eu-central-1.elasticbeanstalk.com/waiter/loginEmployee";
    var headers = new Headers({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/JSON",
    });
    fetch(url, {
      method: "post",
      headers: headers,
      body: JSON.stringify({ username: email, password: password }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.loginStatus) {
          if (!data.employee.registeredBusiness) {
            alert("Bir restorana kayıtlı değilsin.");
          } else {
            history.push("/dashboard", {
              businessId: data.employee.registeredBusiness,
            });
            window.location.reload();
          }
        } else {
          alert("Kayıt bulunamadı.");
        }
      });
    return false;
  };
  return (
    <div className="Home">
      <div className="container">
        <div className="innerWrapper">
          <span className="text">İşletme Email</span>
          <input
            onChange={(e) =>
              setCredentials({ ...credentials, email: e.target.value })
            }
            className="input"
          ></input>
          <span>Şifre</span>
          <input
            type="password"
            onChange={(e) =>
              setCredentials({ ...credentials, password: e.target.value })
            }
            className="input"
          ></input>
          <button
            onClick={() => {
              loginCall();
            }}
            className="buttonContainer"
          >
            Giriş Yap
          </button>
        </div>
      </div>
    </div>
  );
}

export default Login;
