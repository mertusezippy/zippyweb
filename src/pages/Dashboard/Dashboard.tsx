import React, { useEffect, useState } from "react";
import { Left } from "../../components/Left/Left";
import Right from "../../components/Right/Right";
import "./Dashboard.css";
import { useLocation } from "react-router-dom";
import axios from "axios";

export default function Dashboard() {
  const location = useLocation();
  const [campaignData, setCampaignData] = useState<any>({
    discountLimit: "",
    discountAmount: "",
  });
  const [activeCampaigns, setActiveCampaigns] = useState<any>([]);
  const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  const checkActiveCampaigns = async (campaigns: any) => {
    if (campaigns !== null && campaigns !== undefined) {
      for (let i = 0; i < campaigns.length; i++) {
        let campaign = campaigns[i];
        getCampaignDetails(campaign);
      }
      for (let k = 0; k < activeCampaigns.length; k++) {
        setCampaignData({
          ...campaignData,
          discountLimit: getCampaignDescription(activeCampaigns[k]),
          discountAmount: getCampaignAmount(activeCampaigns[k]),
        });
      }
    }
  };
  const getCampaignAmount = (campaign: any) => {
    if (campaign) {
      if (campaign.kind === "CashDiscountCampaign") {
        return campaign.discountAmount + "TL";
      } else if (campaign.kind === "PercentageDiscountCampaign") {
        return "%" + campaign.discountPercentage;
      }
    } else {
      return "";
    }
  };
  const getCampaignDescription = (campaign: any) => {
    if (campaign) {
      return campaign.description;
    } else {
      return "";
    }
  };
  const getBusiness = () => {
    let url = "http://zippyserverappstage-env.eba-wg3eav4w.eu-central-1.elasticbeanstalk.com/business/getBusiness";
    axios
      .get(url, {
        params: { business: location.state.businessId },
      })
      .then((res) => {
        console.log(res);
      });
  };

  const getCampaignDetails = (campaign: any) => {
    let url = "http://zippyserverappstage-env.eba-wg3eav4w.eu-central-1.elasticbeanstalk.com/campaign/getCampaignDetails";
    let now = new Date();
    let hour = now.getHours();
    let minute = now.getMinutes();
    let weekday = weekdays[now.getDay() - 1];
    var headers = new Headers({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/JSON",
    });
    let temp: any = [];
    fetch(url, {
      method: "post",
      headers: headers,
      body: JSON.stringify({ campaignId: campaign }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.availableDays) {
          if (data.availableDays[`${weekday}`]) {
            if (
              data.availableDays[`${weekday}`].start.hour <= hour &&
              data.availableDays[`${weekday}`].end.hour >= hour
            ) {
              if (data.availableDays[`${weekday}`].start.hour < hour) {
                temp.push(data);
              } else if (data.availableDays[`${weekday}`].start.hour == hour) {
                if (data.availableDays[`${weekday}`].start.minute <= minute) {
                  temp.push(data);
                }
              }
            } else if (data.availableDays[`${weekday}`].end.hour > hour) {
              temp.push(data);
            } else if (data.availableDays[`${weekday}`].end.hour == hour) {
              {
                if (data.availableDays[`${weekday}`].end.minute >= minute) {
                  temp.push(data);
                }
              }
            }
          }
        }
      })
      .finally(() => {
        if (temp.length > 0) {
          setActiveCampaigns(temp);
        }
      });
  };
  const getAllCampaigns = () => {
    let url = "http://zippyserverappstage-env.eba-wg3eav4w.eu-central-1.elasticbeanstalk.com/business/getBusinessCampaigns";
    axios
      .get(url, {
        params: { business: location.state.businessId },
      })
      .then((data) => {
        checkActiveCampaigns(data.data.campaigns);
      });
  };
  useEffect(() => {
    getBusiness();
    getAllCampaigns();
  }, []);
  useEffect(() => {
    const interval = setInterval(() => {
      getAllCampaigns();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="dashboard">
      <Left campaigns={activeCampaigns}></Left>
      <Right qr={location.state.businessId}></Right>
    </div>
  );
}
